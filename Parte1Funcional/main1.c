#include <stdlib.h>
#include "AlduinPaarthurnaxIrileth.h"
#include "AniquilamientoPositronicoIonizanteGravitatorio.h"
#include "time.h"
#include <stdbool.h>


bool ColoreoPropio(Grafo G, u32* Coloreo){
  u32 num_vertices = NumeroDeVertices(G);
  u32 vertice, grado, vecino;
  for (u32 i = 0; i < num_vertices; i++) {
    vertice = i;
    grado = Grado(vertice, G);
    for (u32 j = 0; j < grado; j++) {
      vecino = IndiceONVecino(j, vertice, G);
      if (Coloreo[vecino] == Coloreo[vertice]) {
        printf("El coloreo no es propio");
        return false;
      }
    }
  }

  return true;
}

int main(){


  Grafo G = ConstruccionDelGrafo();
  u32 Nvertices = NumeroDeVertices(G);
  u32 Nlados = NumeroDeLados(G);
  u32 delta = Delta(G);
  printf("vertice: %d\n", Nvertices);
  printf("lado: %d\n", Nlados);
  printf("delta: %d\n", delta);


  time_t start;
  time_t end;
  start = time(NULL);
  printf("-----------------------------------\n");

  u32* Coloreo = Bipartito(G);

  if(!ColoreoPropio(G, Coloreo)){
    printf("El coloreo no es propio\n");
  }

  /*for (u32 i = 0; i < Nvertices; i++)
  {
    printf("Color: %d\n", Coloreo[i]);
  }*/
  

  free(Coloreo);
  DestruccionDelGrafo(G);

  end = time(NULL);
  printf("%ld s  %ld m %ld s\n",(end - start), (end - start) / 60, (end - start) % 60 );
  return 0;
}

