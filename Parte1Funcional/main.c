#include <stdlib.h>
#include "AlduinPaarthurnaxIrileth.h"
#include "AniquilamientoPositronicoIonizanteGravitatorio.h"
#include "time.h"

int main(){


  Grafo G = ConstruccionDelGrafo();
  u32 Nvertices = NumeroDeVertices(G);
  u32 Nlados = NumeroDeLados(G);
  u32 delta = Delta(G);
  printf("vertice: %d\n", Nvertices);
  printf("lado: %d\n", Nlados);
  printf("delta: %d\n", delta);


  time_t start;
  time_t end;
  start = time(NULL);
  printf("-----------------------------------\n");


  u32* Orden = calloc(Nvertices, sizeof(u32));
  u32* Coloreo = calloc(Nvertices, sizeof(u32));
  u32 cantidad_colores = 0;

  for (u32 i = 0; i < Nvertices; i++) {
    Orden[i] = i;
  }
  cantidad_colores = Greedy(G, Orden, Coloreo);

  for (u32 i = 0; i < 1058 ; i++) {
    OrdenFromKey(Nvertices, Coloreo, Orden);
    cantidad_colores = Greedy(G, Orden, Coloreo);
  //  printf("%d\n", cantidad_colores);
  }


  free(Coloreo);
  free(Orden);

  DestruccionDelGrafo(G);

  end = time(NULL);
  printf("%d s  %d m %d s\n",(end - start), (end - start) / 60, (end - start) % 60 );
  return 0;
}

/*  for (u32 i = 0; i < Nvertices; i++){
  printf("cantidad de colores = %d\n", cantidad_colores);
  printf("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\n");
  printf("vertice: %d grado: %d\n", Nombre(i, G), Grado(i, G));
  for (u32 j = 0; j < Grado(i, G); j++){
    printf("%d\n", IndiceONVecino(j, i, G));
  }
}
for (u32 k = 0; k < Nvertices; k++){
  printf("%d ", Nombre(k, G));
  printf("-----------------------------------\n");
}

*/
//  u32 *coloreo = Bipartito(G);
